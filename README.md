# Shiny Starter Dashboard

Starter code for a shiny dashboard. Includes standard shiny code, plus ow-specific format, javascript improvements, and modern design.

This is still **under development** but is at the point where you may find it useful. I've copied in a bunch of R files from other dashboards and am now fixing them to be more general to provide examples when you run the app.

Version 0.0.1

# Suggested Use

* Download this into 2 places: one to keep all the code in case you need it (resource copy, update this as the code change on bitbucket), and a second to build a dashboard (working copy).
* Delete from working copy: 1-chart-samples and 2-table-samples folders. Replace these with your own tab folders.
* Copy the chart and table code you want from the resource copy and modify it for use in your working copy.

# Microsoft Visual Studio Code

I write code using many small files in order to effectively organize related code into one place. This makes it easy to jump around the code but only if you have the right IDE.

I highly recommend installing Microsoft Visual Studio Code. 

* When installing, choose the option to add "Open with Code" to contextual options. Open the shiny folder so you can search through all the app files in order to find things. This is not the default option, so take care to choose it. If you forget, you'll need to reinstall in order to pick the option.
* Searching with Code doesn't work well on the network, so work on your local PC when you can (but make sure to backup to the newtwork or bitbucket).