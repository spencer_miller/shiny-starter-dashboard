# This file loops through your server files, which are identifed by starting with s_ and ending in .R.
# Often, order matters, so if you have dependencies put them first by adding 0s to file and folder names, like 0-runfirst/s-0-runfirst.R.
# Server files will be run each time the app is refreshed,
#  so for testing you can load changes by refreshing the browser (F5), no need to stop/start the app.
function (input, output, session) {
  
  for( i in list.files( recursive = TRUE, full.names = TRUE ) ) if( grepl('s-.+[.][Rr]$', i ) ) source( i, local = TRUE )$value
  
  # Run tests, but only if local. We don't want to slow production apps down or cause them to error out before loading.
  if( is_local ) for( i in list.files( 'tests/', recursive = TRUE, full.names = TRUE ) ) if( grepl( '[.][Rr]$', i ) ) source( i, local = TRUE )$value
  
}