/* 
    These functions are useful for formatting numbers in highcharter. To implement, use these or similar:
        dataLabels = list( formatter = JS('function(){ return chartLabel_val(this.y,1); }') )
        hc_yAxis( labels = list( y = 16, formatter = JS('function(){ return chartLabel_val(this.value, 1, 0); }') ) )
 */

// Format as number with commas. 
// https://beta.rstudioconnect.com/barbara/format-numbers/

    function localeString(x, sep, grp) {
        var sx = (''+x).split('.'), s = '', i, j;
        sep || (sep = ',');            // default separator
        grp || grp === 0 || (grp = 3); // default grouping
        i = sx[0].length;
        while (i > grp) {
        j = i - grp;
        s = sep + sx[0].slice(j, i) + s;
        i = j;
        }
        s = sx[0].slice(0, i) + s;
        sx[0] = s;
        return sx.join('.');
    }

// Build chart labels. Mirrors similar functions in R.

    function chartLabel(aspercent){    
        i = this.y ?  this.y : this.total;    
        return chartLabel_val( i, aspercent );
    }
    
    function chartLabel_val( ival, aspercent, digits, no_suffix ){
        
        if( !digits ) digits = 0;
        i = ival;

        if(no_suffix){
            var divxby = 1;
        } else {
            var divxby = i > 1000000000 ? 1000000000 : ( i > 1000000 ? 1000000 : ( i > 20000 ? 1000 : 1 ));
        }    

        if(divxby == 1 && !aspercent ){
            if(i < 10 ){ return i.toFixed(i < .1 ? 4 : 2); }
            return i.toFixed(0).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        // Adjust to percentage or add suffix.
        if(aspercent){ i = (i * 100).toFixed( digits ) + "%"; 

        } else {
            
            // Divide and add the indicator (billion, million, thousand)
            var isuffix = i > 1000000000 ? 'B' : ( i > 1000000 ? 'M' : ( i > 20000 ? 'K' : '' ));
            i = (i / divxby).toFixed(1);
            i = i + ' ' + isuffix; 
            
        }
        
        return i;

    }