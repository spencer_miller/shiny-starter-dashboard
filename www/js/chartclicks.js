$( document ).ready(function() { // Run this code after all elements are loaded.

    // Set up onclick function for map
    $('#avg_inc_occ').click( function() {
        var selections = $('#avg_inc_occ').highcharts().getSelectedPoints().map(function(x) {
            return x.category.name;
        });
        Shiny.onInputChange('claims_selectedOccupations', selections);
    }); 

    // Click action for the sev/freq scatterplot.
    $('#cr_freqsev').click( function() {
        Shiny.onInputChange( 
            'clicked_generalContractor', 
            $('#cr_freqsev').highcharts().getSelectedPoints().map(function(x) {return x.label;})
        );
    }); 
    $('#cr_freqsev_sub').click( function() {
        Shiny.onInputChange( 
            'clicked_subContractor', 
            $('#cr_freqsev_sub').highcharts().getSelectedPoints().map(function(x) {return x.label;})
        );
    }); 

    // Clear selections when the clear button is clicked.
    $('#SB_clearButton').click( function(){
        clearClicks_cr_freqsev();
        clearClicks_cr_freqsev_sub();
        clearClicks_avg_inc_occ();
    });

});

function clearClicks_cr_freqsev(){
    if( $('#cr_freqsev').highcharts() ) {
        $($('#cr_freqsev').highcharts().getSelectedPoints()).each( function(){ this.select(false); });
        Shiny.onInputChange('clicked_generalContractor', []);
    }
}

function clearClicks_cr_freqsev_sub(){
    if( $('#cr_freqsev_sub').highcharts() ) {
        $($('#cr_freqsev_sub').highcharts().getSelectedPoints()).each( function(){ this.select(false); });
        Shiny.onInputChange('clicked_subContractor', []);
    }
}

function clearClicks_avg_inc_occ(){
    if( $('#avg_inc_occ').highcharts() ) {
        $($('#avg_inc_occ').highcharts().getSelectedPoints()).each( function(){ this.select(false); });
        Shiny.onInputChange('claims_selectedOccupations', []);
    }
}