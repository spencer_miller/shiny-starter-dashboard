// v2018.04.05.01

// Remove image smoothing. This fixes an issue with canvas to image being blurry in Firefox and IE.
// https://stackoverflow.com/questions/22003687/disabling-imagesmoothingenabled-by-default-on-multiple-canvases/22018649#22018649
// https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/imageSmoothingEnabled.
    // save old getContext
    var oldgetContext = HTMLCanvasElement.prototype.getContext ;
    // get a context, set it to smoothed if it was a 2d context, and return it.
    function getSmoothContext(contextType) {
        var resCtx = oldgetContext.apply(this, arguments);
        if (contextType == '2d') {
            setToFalse(resCtx, 'imageSmoothingEnabled');
            setToFalse(resCtx, 'mozImageSmoothingEnabled');
            setToFalse(resCtx, 'oImageSmoothingEnabled');
            setToFalse(resCtx, 'webkitImageSmoothingEnabled');  
            setToFalse(resCtx, 'msImageSmoothingEnabled');
        }
        return resCtx ;  
    }
    function setToFalse(obj, prop) { if ( obj[prop] !== undefined ) obj[prop] = false; }
    HTMLCanvasElement.prototype.getContext = getSmoothContext ; // inject new smoothed getContext

// String padding function that works in IE. From https://stackoverflow.com/a/14760377.
String.prototype.paddingLeft = function (paddingValue) {
    return String(paddingValue + this).slice(-paddingValue.length);
};

function rgbfix(x){ return new RGBColor(x); } // this fixes a problem with an external library. DO NOT REMOVE THIS LINE.

// FUNCTIONS FOR SAVING IMAGES.

    // Settings for development.
    var justStyling = 0; // Setting to prevent save and conversion while working on styling before conversion to canvas/image.
    var workSvgCopy = 0; // Prevent deletion of SVG so we can investigate it.
    var stopSvgToCanvas = 0; 
    var removeClone = 0; // 0 if you want to keep the clone to investigate it.
    var resize = 0;

    // Main save image function.
    function saveImg( id, filename, extension ){
        
        if(!filename) filename = 'chart';
        if(!extension) extension = 'png';
        var today = new Date();
        filename = filename + '_' + today.getFullYear() + (today.getMonth() + 1).toString().paddingLeft("00") + today.getDate().toString().paddingLeft("00") + '.' + extension;

        // Clone the div so we can modify it prior to saving.
        var original = document.getElementById(id);
        var clone = original.cloneNode(true); // "deep" clone clones all children.
        clone.id = 'dom_clone';

        // If an element exists with the same id, delete it.
        $('#'+clone.id).remove();

        // Remove potentially problematic elements on any children since these would duplicate ids, and extraneous classes that break shiny.
        $.each(clone.querySelectorAll('div'),function(i,ielement){ 
            ielement.classList.remove("shiny-bound-output"); // Duplicating this class outside Shiny will break Shiny.
            ielement.removeAttribute('id');
        });
        
        // Add to the page so we can convert using html2canvas and select with jQuery.
        document.body.appendChild(clone);

        $('#dom_clone').css('width',$('#'+id).css('width')) // keep the initial width.

        fixClone(clone);

        if( !justStyling || !stopSvgToCanvas ) {
            svgToCanvas(clone);
            // #NEW if(!justStyling){  $.each(clone.querySelectorAll('.highcharts-container'),function(i,idiv){ idiv.parentNode.removeChild(idiv); }); } // Remove any leftover elements after svg-canvas conversion.
        }

        convertAndSave(clone, filename, extension);

    }

    // Fix the clone to prepare it for saving to image.
    function fixClone(clone){

        // Set background to white if it isn't already set.
        jclone = $('#dom_clone');
        if( ['rgba(0, 0, 0, 0)', 'transparent' ].indexOf(jclone.css('background-color')) > -1) jclone.css('background-color','White')
        
        // Remove interactive DataTables elements.
            jclone.find( '.dataTables_length, .dataTables_filter, .dataTables_paginate, .noprint' ).remove();            
            // extra row of sort buttons that comes through DataTables.
            jclone.find( '.dataTables_scrollBody thead' ).css('font-size','0'); // need to keep these in order to determine column widths.            
            // headers with scroll buttons.
            jclone.find( '.dataTables_scrollHead th' ).css('background-image','none'); // https://stackoverflow.com/a/20196738

        // Sizing and border.
        clone.style.display='inline-block';
        clone.style.height='auto';
        var base_font_pt;
        
        // Remove drop down arrows.
        $.each(clone.querySelectorAll( '.item, .selectize-control.single' ),function(i,ielement){ ielement.className += " no-after"; });
        
        // Add OW Copyright. Do this before resizing text.
        jclone.append('<p style="margin:0; padding:0; " class="helper_subscript owcpy">&copy;' + (new Date()).getFullYear() + ' 2018 Marsh Risk Consulting</p>');
        
        $.each(clone.querySelectorAll('.highcharts-container'),function(i,ielement){

            // Prepare zoom. Do this here since we'll need to use it to scale font sizes on the chart. 
            // Select width. We'll take the larger of the actual width and a number of pixels.
            var cwidth = ielement.getBoundingClientRect().width; // current width
            var cheight = ielement.getBoundingClientRect().height;
            var height = Math.max( 1000, cheight );
            var width = Math.ceil( cwidth * height / cheight ); // keep aspect ratio.
            //var width = Math.max( 2000, cwidth );
            //var height = Math.ceil( width / cwidth * cheight ); // keep aspect ratio.

            // Get default font size.
            var iresizeration = Math.min(  cwidth / width, cheight/ height ); // sometimes just the width or height will be changed, take the smallest ratio otherwise things can get too big.
            base_font_pt =  Math.max( 10, Math.min( 18, Math.round( iresizeration  * 12 ) ) );
            var defftsz = (base_font_pt) + 'pt';

            // Text in charts, this is the default, everything else will be modified from here.
            $.each(ielement.querySelectorAll('text'),function(i,ielement){ 
                ielement.style.fontSize = defftsz;
                ielement.style.textDecoration = 'none'; // sometimes text gets underline, this removes it.
                //if( [ 'white', '#fff', '#fffff', 'rgb(255, 255, 255)' ].indexOf( ielement.style.fill.toLowerCase() ) < 0 ){ ielement.style.fill = '#707c80'; } // sometimes text gets blue if it's a link to drill-down, this makes it dark grey to match the rest of the dashboard.
                if( [ 'white', '#fff', '#fffff', 'rgb(255, 255, 255)' ].indexOf( ielement.style.fill.toLowerCase() ) < 0 ){ 
                    ielement.style.fill = '#666666'; // sometimes text gets blue if it's a link to drill-down, this makes it dark grey to match the rest of the dashboard.
                } 
            }); 
            //$.each(ielement.querySelectorAll('.highcharts-legend-item text'),function(i,ielement){ ielement.style.fontSize = '12pt'; }); // Legend items often have more info, need to be smaller.

            // Legend items sometimes overlap, adjust the translate so they don't.
            var priorwidth, priortransx;
            $.each(ielement.querySelectorAll('.highcharts-legend-item'),function(i,ielement){
                if(i==0){
                    inspectelement = ielement;
                    priorwidth =  Math.ceil( ielement.getBoundingClientRect().width );
                    priortransx = Math.ceil( ielement.transform.baseVal[0].matrix.e );
                    priorrow_y = ielement.getBoundingClientRect().y;
                } else {
                    if( ielement.getBoundingClientRect().y == priorrow_y ){ // Only if this is the same row.
                        var thistransy = ielement.transform.baseVal[0].matrix.f;
                        ielement.setAttribute('transform','translate(' + (priortransx + priorwidth + 15) + ',' + thistransy + ')');
                        priorwidth =  ielement.getBoundingClientRect().width;
                        priortransx = ielement.transform.baseVal[0].matrix.e;
                    }
                }
            });
            
            // Make corrections to problematic cahrt elements.
            $.each( ielement.querySelectorAll('text.highcharts-plot-line-label'),function(i,ielement){ ielement.setAttribute('text-anchor','start'); });
            
            // Remove elements we don't want to save/print.
            $.each( ielement.querySelectorAll('.highcharts-scrollbar'), function( i, ielement){ $(ielement).remove(); });
                
            // Sometimes the y axis labels get pushed off the screen, if this happens, bring them back.
            
                // x axis
                var axislabelsbox = ielement.querySelectorAll('.highcharts-xaxis-labels' )[0]; // get the box with labels.
                if( axislabelsbox ){
                    var tx = 0, ty = 0;
                    if( axislabelsbox.getBBox().x < 0 ){ tx = Math.ceil( -axislabelsbox.getBBox().x ); }
                    if( axislabelsbox.getBBox().y < 0 ){ ty = Math.ceil( -axislabelsbox.getBBox().y ); }
                    axislabelsbox.setAttribute( 'transform', 'translate(' + tx + ','+ ty + ')' ); // if it is outside, move it over with translate.
                }
                // y axis
                var axislabelsbox = ielement.querySelectorAll('.highcharts-yaxis-labels' )[0]; // get the box with labels.
                if( axislabelsbox ){
                    var tx = 0, ty = 0;
                    if( axislabelsbox.getBBox().x < 0 ){ tx = Math.ceil( -axislabelsbox.getBBox().x ); }
                    if( axislabelsbox.getBBox().y < 0 ){ ty = Math.ceil( -axislabelsbox.getBBox().y ); }
                    axislabelsbox.setAttribute( 'transform', 'translate(' + tx + ','+ ty + ')' ); // if it is outside, move it over with translate.
                } 

            // Make charting space larger so it is more crisp. This must come LAST so other items come along with the transformation.
            
                ielement.style.width = 'auto';
                ielement.style.height = 'auto'; // this is a good ratio for highcharts, too much height and you get too much white space.
                ielement.style.display='inline-block';

                // Pass these settings onto the any child svgs.
                if( resize ){ $.each(ielement.querySelectorAll('.highcharts-root'),function(j,jelement){
                    jelement.setAttribute('width',width);
                    jelement.setAttribute('height',height);
                    // Fixes highcharts viewBox, for some reason this gets removed sometimes.
                    if(!jelement.hasAttribute('viewBox')){ 
                        // Get the height and width of the main box.
                        var inheight = jelement.querySelectorAll('.highcharts-background')[0].getAttribute('height');
                        var inwidth = jelement.querySelectorAll('.highcharts-background')[0].getAttribute('width');
                        jelement.setAttribute('viewBox','0 0 '+inwidth+' '+inheight); 
                    } 
                }); }
            });

            // Make text larger, relative to the last SVG element that was resized. This is fine if there is just 1 chart, but may be problematic if there are multiple SVGs being resized.

                $.each(clone.querySelectorAll('div'),function(i,ielement){ ielement.style.height = 'auto'; }); // Auto height on divs, expecially for fitting charts and text in as they grow.
                $.each(clone.querySelectorAll('p, .helper_subscript, .filter-note'),function(i,ielement){ 
                    if( resize ){ 
                        ielement.style.fontSize = (base_font_pt * 5 ) + 'pt'; // Various paragraph-type text.
                        ielement.style.margin = '5px';
                        ielement.style.padding = 0;
                        //ielement.style.color = 'Black';
                    }
                }); 
                if( resize ){ $.each(clone.querySelectorAll('.owcpy'),function(i,ielement){ ielement.style.fontSize = (base_font_pt * 5 ) + 'pt'; }); }
                
                // Selectors and chart labels
                $.each(clone.querySelectorAll('.selectize-input.full'),function(i,ielement){ 
                    if( resize ){
                        ielement.style.fontSize = (base_font_pt * 8 ) + 'pt';
                        ielement.style.padding = '25px';
                    }
                    ielement.style.width = 'auto';
                    ielement.className += " no-after";
                    ielement.style.height = "auto";
                    ielement.style.lineHeight = "normal";
                    ielement.style.color = '#006d9e';
                });

        // Remove problematic elements.
        $.each(clone.querySelectorAll(
            'div.gl-container, g.zoomlayer, g.hoverlayer, desc, title, input, script, .load-container, .highcharts-button, .highcharts-tooltip'
        ),function(i,idiv){ idiv.parentNode.removeChild(idiv); });

    }

    // Convert all SVGs to canvas.
    function svgToCanvas(clone){
            
        var svgs = clone.querySelectorAll("svg");
        for(var i=0; i<svgs.length; i++){
        
            var svgImage = svgs[i];

            // Give the SVG specific height/width, otherwise conversion will resize it.
            $(svgImage).attr('width',$(svgImage).width());
            $(svgImage).attr('height',$(svgImage).height());

            // If paths are set to 'currentColor' they will turn black. This happens with icons from font awesome. Explicitly set path colors.
            $.each($(svgImage).find('path'),function(){
                if($(this).attr('fill')=='currentColor') $(this).attr('fill',$(svgImage).css('color'));
            })
            
            // Save style items that canvas conversion doesn't catch.
            var css_save = [{ css: 'vertical-align' }];
            for( var j = 0; j < css_save.length; j++) css_save[j].val = $(svgImage).css(css_save[j].css);

            // Create a canvas and insert it into the DOM.
            var $canvas = $('<canvas/>');
            $canvas.insertAfter(svgImage);
            canvg(
                $canvas.get(0),
                (new XMLSerializer()).serializeToString(svgImage),
                { ignoreMouse: true, ignoreAnimation: true }
            ); // paint the canvas with your SVG using 

            for( var j = 0; j < css_save.length; j++) if(css_save[j].val) $canvas.css(css_save[j].css,css_save[j].val);
        
            // remove the SVG image.
            if(!workSvgCopy){ svgImage.parentNode.removeChild(svgImage); } 
        }

    }

    // Convert to canvas and save.
    function convertAndSave(clone, filename, extension){

        html2canvas(clone, { onrendered: function (canvas) {
            
            if( resize ){

                // Make the canvas smaller so it is more friendly with other programs like PPT, etc.
                var iheight = canvas.height;
                var iwidth = canvas.width;
                var newheight = Math.min( 600, canvas.height );
                var newwidth = iwidth * newheight / iheight;
                
                // https://stackoverflow.com/questions/11397785/html2canvas-resize
                var extra_canvas = document.createElement("canvas");
                extra_canvas.setAttribute('width',newwidth);
                extra_canvas.setAttribute('height',newheight);
                var ctx = extra_canvas.getContext('2d');
                ctx.drawImage( canvas, 0, 0, canvas.width, canvas.height, 0, 0, newwidth, newheight );

            }
                    
            if(!justStyling){
                if( extension == 'pdf' ){
                    canvas.toBlob(function (blob) {
                        var url = window.URL || window.webkitURL;
                        var imgSrc = url.createObjectURL(blob);
                        var img = new Image();
                        img.src = imgSrc;
                        img.onload = function () {
                            var pdf = new jsPDF('p', 'px', [img.height, img.width]);
                            pdf.addImage(img, 0, 0, img.width, img.height);
                            pdf.save(filename);
                        }; 
                    });
                } else {
                    canvas.toBlob(function(blob) { saveAs(blob, filename); }, "image/png" );
                }
                clone.parentNode.removeChild(clone); // remove the cloned version from the document.
            }
        }});

    }

    
// Code I am saving just in case I need it later:
        
    // Plotly fix, move all the svgs into the first svg.
    // process all svg-containing plotly divs.
    /*$.each(clone.querySelectorAll("div.plot-container.plotly > div.svg-container"),function(i,idiv){
        // Move any SVGs after the first into the first so they don't get converted to separete canvases.
        var isvgs = idiv.querySelectorAll('svg');
        if(isvgs.length>1){ for(var j=1; j<isvgs.length; j++){
            while (isvgs[j].childNodes.length) { isvgs[0].appendChild(isvgs[j].firstChild); }
            isvgs[j].remove();
        }}
    });*/