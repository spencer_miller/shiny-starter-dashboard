// JS code to fix issues with highcharts.

// Fix height of highcharts.

    function fixHeight(outputid){
        
        // Applying this to some charts can make them bounce around, so only run on specific charts that need the height fixed.
        $('#' + outputid + ' .highcharts-container').each( function(i, ielem){
            $(ielem).parent().css( 'height', $(ielem).css('height') );
        });
        
    }

// This event runs a lot, but after plots so it is the only way to check final plot status.

    $(document).on('shiny:conditional',function(event){ 
        
        // Add notes about no data available for charts.
        $('.active .highchart.html-widget.html-widget-output.shiny-bound-output').each( function(i, ielem ){

            ielem = $(ielem);
            ielem.parent().children('.no-data-alert').remove(); // remove any existing alerts.

            if( ielem.css('visibility') == 'hidden' ){
                ielem.before( '<p class="no-data-alert" >No data available for this chart. Please try a different selecton.</p>' ); // if applicable, add one.
                //$(this).css('display','none'); // For some reason, chart elements are coming through when visibility is hidden, this fixes that.
            }

        });
        
        // Fix height of highcharts.
        // Applying this to some charts can make them bounce around, so only run on specific charts that need the height fixed.
        fixcharts = [ 'es_gauge', 'cr_risk_category', 'corrective_action_party', 'cr_action_status_pct', 'es_bar', 'es_pie1', 'es_pie2', 'es_claimDist_chart' ];
        for( var i = 0; i < fixcharts.length; i++) fixHeight(fixcharts[i]);

    });

// Run this code after all elements are loaded.

    $( document ).ready( function() { 
        
        // Fix highcharts options.
        Highcharts.setOptions({ lang: { drillUpText: '<' } });
        
        // Fix workaround to fix issues with highcharts and transform: scale(). From https://github.com/highcharts/highcharts/issues/2405.
        Highcharts.wrap(Highcharts.Pointer.prototype, 'normalize', function (proceed, event, chartPosition) {
            var e = proceed.call(this, event, chartPosition);

            var element = this.chart.container;
            if (element && element.offsetWidth && element.offsetHeight) {

                var scaleX = element.getBoundingClientRect().width / element.offsetWidth;
                var scaleY = element.getBoundingClientRect().height / element.offsetHeight;
                if (scaleX !== 1) {
                    e.chartX = parseInt(e.chartX / scaleX, 10);
                }
                if (scaleY !== 1) {
                    e.chartY = parseInt(e.chartY / scaleY, 10);
                }

            }
            return e;
        });

    });