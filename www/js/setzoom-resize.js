$( document ).ready( function() { 
    
    var oldsize = -1;

    var elem = $('body');

    newsize = elem.width();

    if( oldsize != newsize ) {

        var zoom = Math.min( 100, Math.floor( $('body').width() / 1903 * 100 ) / 100 );
        elem.css('transform-origin', 'top left' );
        //elem.css('transform', 'scale('+zoom+' '+zoom+')' );
        elem.css('transform', 'scale('+zoom+', '+zoom+')' );
        elem.css('width', '1900px' );        
        oldsize = newsize;

    }

});