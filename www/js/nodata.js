// This code manages the 'no data available' alerts.
$(document).on('shiny:inputchanged',function(event){
        
    // Add notes about no data available for charts.
    $.each($('.active .highchart'),function(){ 
        $(this).parent().children('.no-data-alert').remove(); // remove any existing alerts.
        if($(this).css('visibility')=='hidden'){
            $(this).before('<p class="no-data-alert" >No data available for this chart. Please try a different selecton.</p>'); // if applicable, add one.
        }
    });

});
// Remove alerts whenever items are being recalculated.
$(document).on('shiny:recalculating',function(event){    
    $.each($('.active .highchart'),function(){ $(this).parent().children('.no-data-alert').remove(); });
});