// This file handles updates of various filter strings.
// We do it this way so we don't need to create these for each chart. Instead, we create it once (hidden) in Shiny and then use JS and HTML to place it elsewhere.

    $(document).on('shiny:inputchanged',function(event){ // Function that runs any time a visual is shown.
        
        // Refresh the filter notes.
            
            $('.filter-note').each(function(){ $(this).html(
                '<p class="helper_subscript"><span style="color: #a6a6a6">Viewing </span>' + $('#filterNote').text() + '</p>'
            );});

        // You can add other filter notes similarly.

    });