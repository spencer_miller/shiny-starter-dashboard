$( document ).ready( function() { 
    
    // Client logo.
    //     logo_container is necessary for sizing SVG but may not be necessary for other logos. 
    //     To get the best image quality, use the actual dimensions of your image (you may need to resize the image for this to work).

        $('.logo').html(
            '<div class="logo_container inline"><img src="client-logo.png" style="width: 100%; height: auto; " /></div>'
        );         

    // Our logo.

        $('#tabset').append(
            '<div style="float: right; z-index: 999; height: 100%; position: relative; top: 19px; ">' +
                '<div class="inline bottom"><img src="ow-logo.png" style="position: relative; top: -3px; margin-left: 5px; margin-right: 5px; width:170px; height:auto; text-align:right; display:inline; " alt="Oliver Wyman Logo"/></div>'+  
            '</div>'
        );
    
    // Favicon (the icon on the tab next to tab name). Google favicon for more info.

        $('title').append( '<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">' );

});