table_ui <<- function( id, downloadbutton = TRUE, filterNote_class = NULL, table_minheight = 100 ) div( class='table_container',

  if( !nanull( filterNote_class ) ) div( class = filterNote_class ), # this class identifier will be updated by javascript with the value of filterNote() reactive.

  div( style = paste0('min-height: ',table_minheight,'px'), dataTableOutput(id) ),

  if( downloadbutton ) downloadButton( paste0(id,'_download'), "Download" )

)
