
# UI files only run when the app starts or this file changes.
# It is helpful to have the version number here so you can change it trigger reload of all ui files. 
# Often, I'll just add random letters or numbers to it, and only clean it up when I am ready to actually load it in.
# This version number also gets printed at the bottom left of the UI so you know which version your users are on.

  version_num = '2018.06.07.016' # having this here also ensures UI files get reloaded when version changes.

# We use shinydashboard structure, but you can use a different structure if you like.
  
  dashboardPage(
    
    dashboardHeader(), # Nothing here, we actually use javascript to set the header since that seems to work better.
    
    dashboardSidebar(
  
      # A special note that identifies the filters you have selected. 
      # Javascript will populate this to anywhere you use the class: filter-note
      # This is much easier than creating a uiOutput for each chart.
      # The default functionality is implemented at 0-general/s-filterinfo.R and www/js/filter-strings.js
      uiOutput("filterNote"), 
  
      # Allow the use of shiny js. Comment this out if you aren't using shinyjs.
      useShinyjs(),
    
      # Run ui code which is identifed by starting with ui_ and ending in .R.
      # Often, order matters, so if you have dependencies put them first by adding 0s to file and folder names, like 0-runfirst/u-0-runfirst.R.
      # These will only run when this file changes (see note about version_num above) or when the app starts.
      {
        for( i in list.files( recursive = TRUE, full.names = TRUE ) ) if( grepl('u-.+[.][Rr]$', i ) ) source( i, local = TRUE )$value
      },
  
      # Here are some optional sidebar features you can use.
      # You'll need to implement your server code to make them fully functional. Or remove/comment them if you don't plan to use.
      
        # Demo warning.
        # Comes in handy when you aren't quite finished yet.
        div( style="display: inline-block; height: 100%; ", 
             p(
               style="margin: 10px; margin-bottom: 0px; padding:5px; background-color: #ff4d4d; color: White; font-size: 9pt; width: 120px; text-align: center;" ,
               class="helper_subscript", 
               "FOR DEMO ONLY" 
             )
        ),
      
        # Button: clear filters.
        div(id = "SB_clearButton_div",style='margin-top:20px;',
          div( style='display:inline-block', actionButton( inputId = "SB_clearButton",label = "Clear Filters" ) )
        ),
  
        # Helper bubble about sidebarInput filters.
        tags$p(
          class = 'popup-note', style = 'padding: 0; margin: 0; margin-left: 140px; margin-top: 15px; ',
          title = '"-" toggles exclusion filter (as opposed to the default inclusion).', '?' # jquery UI will create a pop-up with the 'title' text. See https://jqueryui.com/tooltip/.
        ), 
      
      # Add any other sidebar items here (or elsewhere if you feel comfortable).
      
  
      # This side bar function is defined in 0-general/u-sidebar.R using the filters object set up in global.R.
      # Go there to specify those values.
      ui_sidebar(filters)
  
    ), # /dashboardSidebar
    
    dashboardBody(
  
      # This is a special function defined at 0_general/u_head.R. 
      # It brings in your CSS, JS, etc. without you having to specifically tell it what is there.
      # In general, you won't change this function.
      # This is where we set the tab title you see in the browser.
      ui_head( 'Your Dashboard' ),
  
      # Create the main tabset.
      tabsetPanel( id = "tabset", type = "tabs",
        
        # Optionally, select a tab. This is helpful for testing. Otherwise, it'll select the first tab. Use the Value, or title if value isn't set.           
        # selected = 'pc', # ifelse( is_local, 'cr', 'es' ),
  
          tabPanel( 
            title = tagList( 
              icon( "chart-bar" ),  # use shiny icon to add an icon. We override font awesome with version 5, so be aware of this if using font-awesome.
              HTML( "&nbsp; CHARTS" )
            ) , # &nbsp; is a forced space in HTML. This creates space after the icon.
            value = 'charts', 
            fluidRow( 
              chartsamples() # defined in 1-chart-samples/u-0-chart-samples.R.
          )),
        
        tabPanel( 
          title = tagList( 
            icon( "table" ),  # use shiny icon to add an icon. We override font awesome with version 5, so be aware of this if using font-awesome.
            HTML( "&nbsp; TABLES" )
          ),
          value = 'tables', 
          fluidRow( 
            p( 'TODO' )
          ))
  
      ),
  
      # Put in the version number so you can easily tell if your users are on the latest version.
      p( class = 'helper_subscript right', version_num ),
      p( class = 'helper_subscript left', style = 'position: relative; top: -11px; ', HTML( paste0( '&copy; ',year(Sys.Date()), ' Oliver Wyman Actuarial Consulting' ) ) )
  
  ))
