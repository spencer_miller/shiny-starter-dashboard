output[['donut-pie']] <- renderHighchart({

    # Start progress bar.
    proginit('bar-detailhover-scroll')

    # Get the data for this chart.
    # This reactive is defined in 0-general/s-data.R.
    idt = data_r()

    # Get categories and values.
    icatg = data_r()$region
    ivals = data_r()$Population
    ivalname = 'Population'
        
    # Combine val, catg to get a data frame we can work with.
    # Group by category and get the sum of value.
    cdt = data.frame(
        val = ivals,
        catg = icatg,
        stringsAsFactors = FALSE
    ) %>% group_by( catg ) %>% summarize( val = sum(val) ) %>% ungroup()

    # Optional: Group small buckets into 'Other'

        # Add other for smaller than 3%.
        cdt$catg[ cdt$val / sum(cdt$val) < .03 ] <- 'Other'
        # Regroup now that we have 'Other'.
        cdt %<>% group_by(bucket) %>% summarize( value = sum(value) ) %>% ungroup() 
        cdt %<>% arrange( ifelse( bucket == 'Other', -1, value ) )
    
    # Choose color and number format.
    icolors = mmc_colors[ 4, ] # donut needs multiple colors.
    iformat = fmat$percentage

    proginc('building chart')

    ichart <- highchart() %>%
        hc_chart( 
            marginTop = 0, 
            marginBottom = 0, 
            marginLeft = 0, 
            marginRight = 0, 
            height = 320,
            width = 600
        ) %>% 
        hc_xAxis( 
            categories = icatg, 
            labels= list( style = list( fontSize ='8pt', fontFamily = 'Open Sans' ) , enabled = TRUE ),
            title = list( enabled = FALSE ) # I prefer to do titles in HTML/ui.
        ) %>%
        hc_yAxis( 
            title = list( enabled = FALSE )
        ) %>%
        hc_plotOptions(
            series = list(
                animation = FALSE,
                innerSize = '50%', # make this 0 (or remove it?) to do pie.
                colors = icolors
            )
        ) %>%
        hc_legend( 
            enabled = TRUE,
            itemStyle = list( fontWeight = 'normal', fontFamily = 'Work Sans', color = '#6b777b', fontSize = '1em' )
        )

    ichart %<>% hc_add_series( 
        data = data.frame( y = ivals, name = icatg ), type = "pie", name = ivalname,
        dataLabels = list(
            formatter = JS( 'function(){ return this.key + ": " + chartLabel_val(this.y) + " (" + chartLabel_val(this.percentage/100,1) + ")"; }' ),
            enabled = TRUE, overflow = 'justify', distance = 10, useHTML = FALSE, # if you use HTML, the chart won't resize right when saving as picture (even though it looks better on screen).
            style = list( fontWeight = 'normal', fontFamily = 'Rubik', color = '#6b777b', fontSize = '8pt', textOutline = '0px' )
        ),
        tooltip = list( enabled = FALSE, pointFormat = iformat$pointFormat )
    )

    progclose()

    return(ichart)

})