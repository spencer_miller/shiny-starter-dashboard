# See .js file www/map.js for how we make this map responsive to clicks.
# At a high level, each click of the map updates input$selectedStates with a list of selected states.

output[['map']] <- renderHighchart({

    # Start progress bar.
    proginit('map')

    # Get the data for this chart.
    # This reactive is defined in 0-general/s-data.R.
    idt = data_r()

    # Increment progress bar.
    proginc('settup up chart')

    # Set up the chart.
        
        # Number format, see 0-global/s-formats.R
        iformat = fmat$percentage

        # Color.
        icolor = mmc_colors[ 3, 3 ]

        # For the map, we need the ycol as a string.
        ycol = 'Murder'
    
        # Highcharts doesn't like spaces in the names for maps, so we use make.names here.
        colnames(idt) <- make.names( colnames(idt) )
        ycol = make.names(ycol)

    # Increment progress bar.
    proginc('building chart')
    
    ichart = highchart() %>% 
        hc_mapNavigation( 
            enableMouseWheelZoom = TRUE,
            enableButtons = TRUE 
        ) %>%
        hc_chart( 
            height = 320,
            width = 500
        ) %>% 
        hc_add_series_map(
            usamap, # must read in usamap.RDS, this is done in global.
            idt, 
            name = ycol,
            borderColor = icolor,
            allowPointSelect = TRUE,
            nullColor = "#e3e8e8",
            value = ycol,
            borderWidth = 0.1,
            joinBy = c("hc-a2", "abb"),
            states = list(
                select = list( borderColor = 'black', borderWidth = 2, shadow = FALSE, enabled = TRUE ), 
                hover = list( brightness = 0 )
            ),
            tooltip = iformat$tooltip
        )
    
    # Heat axis.
    ichart %<>% 
        hc_colorAxis(
            min = min( idt[[ycol]], na.rm = T ), max = max( idt[[ ycol ]], na.rm = T ),
            minColor = '#ffffff', maxColor = icolor, type = ' '
        )

    # Close the progress bar.
    progclose()

    return(ichart)

})