# Chart function for line and bar charts.
output[['column-drilldown']] <- renderHighchart({

    # Start progress bar.
    proginit('column-drilldown')

    # Get the data for this chart.
    # This reactive is defined in 0-general/s-data.R.
    idt = data_r()

    # Summarize by region. This will be the top-level chart data.
    # Ungroup converts to a regular tibble (not grouped) which can prevent errors downstream.
    idtsumm = idt %>% group_by( region ) %>% summarize( Population = sum(Population), Income = sum(Income) ) %>% ungroup()

    # Pick colors.
    colors = c( mmc_colors[ 1, 2 ], mmc_colors[ 2, 2 ] )

    # Close progress indicator.
    progclose()

    return(highchart(list(
        chart = list(
            type = "column", 
            height = 320, 
            width = 1200
        ),
        credits = list( enabled = FALSE ),
        title = list( enabled = FALSE, text = NULL ),
        xAxis = list(
            type = 'category',
            labels = list( style = list( fontWeight = 'normal', fontFamily = 'WorkSans', color = '#6b777b', fontSize = '11pt' ) )
        ),

        # Two scale axes.
        yAxis = list( 
            list( labels = list( enabled = FALSE), title = list( enabled = FALSE ), gridLineDashStyle = NULL, gridLineWidth = 0 ),
            list( labels = list( enabled = FALSE), title = list( enabled = FALSE ), gridLineDashStyle = NULL, gridLineWidth = 0 )
        ),

        legend = list( enabled = TRUE, itemStyle = list( fontWeight = 'normal', fontFamily = 'WorkSans', fontSize = '11pt' ) ),
        plotOptions = list(
            series = list(
                animation = FALSE, 
                dataLabels = list( 
                    enabled = TRUE,
                    style = list( color = 'black',  textOutline = FALSE, fontFamily = 'Rubik', fontSize = '11pt', fontWeight = 'normal' )
                )
            )
        ),

        exporting = list( buttons = list( 
            contextButton = list( enabled = FALSE ),
            printButton = list( enabled = FALSE )
        )),

        series = list(

            list( 
                name = 'Population', 
                color = colors[1], 
                data = lapply( idtsumm$region, function(i){ return(list(
                    name = i, 
                    y = idtsumm$Population[ idtsumm$region == i ],
                    dataLabels = list( formatter = JS( 'function(){ return chartLabel_val(this.y); }' ) ), # chartLabel_val defined at js/chartLabel.js
                    drilldown = paste0( i, '-1' ) # can't have duplicate drilldown ids.
                ))})
            ),

            list( 
                name = 'Income', 
                color = colors[2], 
                yAxis = 1, # second scale axis.
                data = lapply( idtsumm$region, function(i){ return(list(
                    name = i, 
                    y = idtsumm$Income[ idtsumm$region == i ],
                    dataLabels = list( formatter = JS( 'function(){ return chartLabel_val(this.y); }' ) ), # chartLabel_val defined at js/chartLabel.js
                    drilldown = paste0( i, '-2' ) # can't have duplicate drilldown ids.
                ))})
            )
        ),

        # Define drill-downs.
        drilldown = list( 

            series = c(
            
                lapply( idtsumm$region, function(j){
                    jdt = idt %>% filter( region == j ) %>% group_by( division ) %>% summarize_at( vars(Population, Income), funs(sum) ) %>% ungroup()
                    return(
                        list(
                            id = paste0( j, '-1' ), # can't have duplicate drilldown ids.              
                            name = paste( 'Population: ', j ), 
                            color = colors[1],
                            categories = jdt$division,
                            data = lapply( jdt$division, function(k){ return( list( name = k, y = jdt$Population[ jdt$division==k ] ) ) } ),
                            dataLabels = list( formatter = JS('function(){ return chartLabel_val(this.y); }') )
                        )
                    )
                }),
                
                lapply( idtsumm$region, function(j){
                    jdt = idt %>% filter( region == j ) %>% group_by( division ) %>% summarize_at( vars(Population, Income), funs(sum) ) %>% ungroup()
                    return(
                        list(
                            id = paste0( j, '-2' ),
                            name = paste('Income: ', j ),
                            color = colors[2],
                            categories = jdt$division,
                            data = lapply( jdt$division, function(k){ return( list( name = k, y = jdt$Income[ jdt$division==k ] ) ) } ),
                            dataLabels = list( formatter = JS('function(){ return chartLabel_val(this.y); }') )
                        )
                    )
                })

            )

        )

    )))

})